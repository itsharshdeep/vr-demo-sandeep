﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandeep.VrDemo
{
	public class GameController : MonoBehaviour
	{
		#region Static  Instance

		private static GameController instance;

		public static GameController Instance {
			get { 
				return instance;
			}
		}

		#endregion

		#region Variables

		[Header ("Dome 360")]
		[SerializeField] private MeshRenderer dome360;

		[Header ("Highlight selectables")]
		[SerializeField] private HightlightSelectable[] highlightSelectables;

		[Header ("Domes Assets")]
		[SerializeField] private GameObject dome1Assets;
		[SerializeField] private GameObject dome2Assets;

		#endregion

		#region Init

		// Use this for initialization
		void Start ()
		{
			instance = this;
		}

		#endregion

		#region Update

		// Update is called once per frame
		void Update ()
		{

		}

		#endregion

		#region Deselect all highlights

		public void DeslectAllHighlights ()
		{
			foreach (var item in highlightSelectables) {
				item.DeSlectObject ();
			}
		}

		#endregion

		#region Dome Selection

		public void SelectDome (int domeNumber)
		{

			dome360.material.SetTexture("_MainTex",Resources.Load<Texture> ("Textures/" + domeNumber));

			switch (domeNumber) {
			case 1:
				dome1Assets.SetActive (true);
				dome2Assets.SetActive (false);
				break;
			case 2:
				dome1Assets.SetActive (false);
				dome2Assets.SetActive (true);
				break;

			default:
				break;
			}
		}

		#endregion

	}
}
