﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandeep.VrDemo
{
	public class PlayerController : MonoBehaviour
	{
		public float zoom = 2;
		private bool isZoomed = false;
		// Use this for initialization
		void Start ()
		{
		
		}
	
		// Update is called once per frame
		void Update ()
		{
			RaycastHit hit;
			Debug.DrawRay (transform.position, transform.forward);
			if (Physics.Raycast (transform.position, transform.forward, out hit, 1000)) {
				GameController.Instance.DeslectAllHighlights ();
				switch (hit.transform.name) {
				case "360Dome":
					if (Input.GetMouseButtonDown (0)) {
						Debug.Log ("Input down");
						if (!isZoomed) {
							transform.root.localPosition = transform.position + (transform.forward * zoom);
							isZoomed = true;
						} else {
							transform.root.localPosition = transform.position - (transform.forward * zoom);
							isZoomed = false;
						}
					}
						

					break;
				case "Lake":
				case "GolfCart":
					hit.transform.GetComponent<HightlightSelectable> ().SelectObject ();
					hit.transform.GetComponent<MeshRenderer> ().enabled = true;
					break;

				default:
					break;
				}

			}
		
		}

	}
}