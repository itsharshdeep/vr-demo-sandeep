﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandeep.VrDemo
{
	public class HightlightSelectable : HighlighterController
	{
		[SerializeField] private Color color;
		[SerializeField] private GameObject distance;
		// Use this for initialization
		void Start ()
		{
		
		}

		internal void SelectObject ()
		{
			h.On (color);
			distance.SetActive (true);
		}

		internal void DeSlectObject ()
		{
			h.ConstantOff ();	
			GetComponent<MeshRenderer> ().enabled = false;
			distance.SetActive (false);
		}
	}
}